package de.elementil.arzreader.example;

import java.io.IOException;
import java.nio.file.Paths;

import de.elementil.arzreader.ArzReader;
import de.elementil.arzreader.RecordFinder;
import de.elementil.arzreader.model.ArzFile;

public class ArzReaderExample {

	private static final String ARZ_PATH = "D:\\Steam\\steamapps\\common\\Grim Dawn\\database\\database.arz";
	private static final String DUMP_PATH = "C:\\GD Database extracted";

	private static final String DEVOTION_STARS_PATH = "records/skills/devotion";
	private static final String PERCENT_FIRE_DAMAGE = "offensiveFireModifier";
	private static final String FILE_DESCRIPTION = "FileDescription";

	public static void main(String[] args) throws IOException {

		// create a new instance of ArzFile with either a String or a File
		// object containing the path to the database.arz
		ArzFile arzFile = new ArzFile(ARZ_PATH);

		// create a new instance of ArzReader
		ArzReader reader = new ArzReader();

		// read the data from the database.arz
		reader.read(arzFile);

		RecordFinder devotionRecordFinder = arzFile.recordFinder().recordPath(DEVOTION_STARS_PATH);

		// find all devotion stars that buff % fire damage
		devotionRecordFinder.property(PERCENT_FIRE_DAMAGE).isGreaterThan(0).find().forEach(record -> System.out.println(record));

		// find all devotion stars of the 'Imp' constellation
		devotionRecordFinder.property(FILE_DESCRIPTION).contains("Imp").find().forEach(record -> System.out.println(record));

		// dump the data to the filesystem into a folder of your choice
		reader.dump(arzFile, Paths.get(DUMP_PATH));
	}
}
