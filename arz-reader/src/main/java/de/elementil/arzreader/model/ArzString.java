package de.elementil.arzreader.model;

public class ArzString {
	private final int length;
	private final String string;

	public ArzString(String string, int length) {
		this.string = string;
		this.length = length;
	}

	public int getLength() {
		return length;
	}

	public String getString() {
		return string;
	}

	@Override
	public String toString() {
		return "ArzString [length=" + length + ", string=" + string + "]";
	}
}
