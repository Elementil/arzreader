package de.elementil.arzreader.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonValue;

public class ArzRecord {

	private List<ArzRecordProperty> properties = new ArrayList<ArzRecordProperty>();	
	private String fileName;
	private int typeLength;
	private String type;
	private int dataOffset;
	private int dataSizeCompressed;
	private int dataSizeDecompressed;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getTypeLength() {
		return typeLength;
	}

	public void setTypeLength(int typeLength) {
		this.typeLength = typeLength;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getDataOffset() {
		return dataOffset;
	}

	public void setDataOffset(int dataOffset) {
		this.dataOffset = dataOffset;
	}

	public int getDataSizeCompressed() {
		return dataSizeCompressed;
	}

	public void setDataSizeCompressed(int dataSizeCompressed) {
		this.dataSizeCompressed = dataSizeCompressed;
	}

	public int getDataSizeDecompressed() {
		return dataSizeDecompressed;
	}

	public void setDataSizeDecompressed(int dataSizeDecompressed) {
		this.dataSizeDecompressed = dataSizeDecompressed;
	}

	public List<ArzRecordProperty> getProperties() {
		return properties;
	}

	public void setProperties(List<ArzRecordProperty> properties) {
		this.properties = properties;
	}

	@JsonValue
	public Map<String, Object> getPropertyMap() {
		// @formatter:off
		return properties.stream().collect(
				Collectors.toMap(ArzRecordProperty::getName,
						ArzRecordProperty::getValue,
						(u, v) -> {	throw new IllegalStateException(String.format("Duplicate key %s", u)); },
						LinkedHashMap::new));
		// @formatter:on
	}

	public boolean isInitialized() {
		return !properties.isEmpty();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ArzRecord " + getFileName()).append(System.lineSeparator());
		properties.forEach(property -> sb.append(property).append(System.lineSeparator()));
		return sb.toString();
	}
}
