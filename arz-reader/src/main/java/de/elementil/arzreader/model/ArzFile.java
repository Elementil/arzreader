package de.elementil.arzreader.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.elementil.arzreader.RecordFinder;

public class ArzFile {

	private final File file;
	private short unknown;
	private short version;
	private int recordTableStart;
	private int recordTableSize;
	private int recordTableEntries;
	private int stringTableStart;
	private int stringTableSize;

	private List<ArzString> strings;
	private List<ArzRecord> records;

	/**
	 * Creates a new ArzFile instance
	 * 
	 * @param path
	 *            the path to the .arz file
	 * @throws NullPointerException
	 *             If the path argument is null
	 */
	public ArzFile(String path) throws NullPointerException {
		this(new File(path));
	}

	/**
	 * Creates a new ArzFile instance
	 * 
	 * @param file
	 */
	public ArzFile(File file) {
		this.file = file;
		strings = new ArrayList<ArzString>();
		records = new ArrayList<ArzRecord>();
	}

	public RecordFinder recordFinder() {
		RecordFinder finder = new RecordFinder(records);
		return finder;
	}

	public List<ArzString> getStrings() {
		return strings;
	}

	public void setStrings(List<ArzString> strings) {
		this.strings = strings;
	}

	public String getString(int index) {
		return strings.get(index).getString();
	}

	public List<ArzRecord> getRecords() {
		return records;
	}

	public void setRecords(List<ArzRecord> records) {
		this.records = records;
	}

	public ArzRecord getRecord(int index) {
		return records.get(index);
	}

	public File getFile() {
		return file;
	}

	public short getUnknown() {
		return unknown;
	}

	public void setUnknown(short unknown) {
		this.unknown = unknown;
	}

	public short getVersion() {
		return version;
	}

	public void setVersion(short version) {
		this.version = version;
	}

	public int getRecordTableStart() {
		return recordTableStart;
	}

	public void setRecordTableStart(int recordTableStart) {
		this.recordTableStart = recordTableStart;
	}

	public int getRecordTableSize() {
		return recordTableSize;
	}

	public void setRecordTableSize(int recordTableSize) {
		this.recordTableSize = recordTableSize;
	}

	public int getRecordTableEntries() {
		return recordTableEntries;
	}

	public void setRecordTableEntries(int recordTableEntries) {
		this.recordTableEntries = recordTableEntries;
	}

	public int getStringTableStart() {
		return stringTableStart;
	}

	public void setStringTableStart(int stringTableStart) {
		this.stringTableStart = stringTableStart;
	}

	public int getStringTableSize() {
		return stringTableSize;
	}

	public void setStringTableSize(int stringTableSize) {
		this.stringTableSize = stringTableSize;
	}
}
