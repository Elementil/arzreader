package de.elementil.arzreader.model;

public class ArzRecordProperty {

	private String name;
	private Object value;

	public ArzRecordProperty(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return name + "=" + value;
	}
}
