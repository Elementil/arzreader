package de.elementil.arzreader;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.elementil.arzreader.annotation.ComplexProperty;
import de.elementil.arzreader.annotation.MappedArzRecord;
import de.elementil.arzreader.annotation.PropertyName;
import de.elementil.arzreader.annotation.PropertyValue;
import de.elementil.arzreader.model.ArzRecord;
import de.elementil.arzreader.model.ArzRecordProperty;

public class ObjectMapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectMapper.class);

	public <T> T map(ArzRecord record, Class<T> clazz) {
		if (clazz.getAnnotation(MappedArzRecord.class) != null) {
			try {
				T target = clazz.newInstance();
				for (Field field : clazz.getDeclaredFields()) {
					field.setAccessible(true);
					if (field.isAnnotationPresent(PropertyName.class)) {
						setFieldValue(record, field, target);
					} else if (field.isAnnotationPresent(ComplexProperty.class)) {
						if (Collection.class.isAssignableFrom(field.getType())) {
							Type collectionType = field.getGenericType();
							if (collectionType instanceof ParameterizedType) {
								ParameterizedType parameterizedType = (ParameterizedType) collectionType;
								Class<?> complexPropertyType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
								Set<Object> complexProperties = new HashSet<Object>();
								for (ArzRecordProperty property : record.getProperties().stream()
										.filter(property -> isGreaterThanZero(property)).collect(Collectors.toList())) {
									complexProperties.add(mapComplexProperty(complexPropertyType, property));
								}
								field.set(target, complexProperties);
							}
						} else if (Object.class.isAssignableFrom(field.getType())) {
							ComplexProperty propertyName = field.getAnnotation(ComplexProperty.class);
							String propertyNameValue = propertyName.value();
							if (StringUtils.isBlank(propertyNameValue)) {
								propertyNameValue = field.getName();
							}
							field.set(target,
									mapComplexProperty(field.getType(), getProperty(record, propertyNameValue)));
						}
					}
				}
				return target;
			} catch (InstantiationException | IllegalAccessException e) {
				LOGGER.error("Could not create a new instance of type " + clazz.getName(), e);
				throw new RuntimeException("Could not create a new instance of type " + clazz.getName(), e);
			}
		} else {
			LOGGER.error("Targeted type is not annotated as MappedArzRecord");
			throw new IllegalArgumentException("Targeted type is not annotated as MappedArzRecord");
		}
	}

	private boolean isGreaterThanZero(ArzRecordProperty property) {
		if (property.getValue() instanceof Float) {
			return (float) property.getValue() > 0;
		}
		return false;
	}

	private <T> void setFieldValue(ArzRecord record, Field field, T target) {
		PropertyName propertyName = field.getAnnotation(PropertyName.class);
		String propertyNameValue = propertyName.value();
		if (StringUtils.isBlank(propertyNameValue)) {
			propertyNameValue = field.getName();
		}

		Object propertyValue = getProperty(record, propertyNameValue).getValue();
		try {
			field.set(target, propertyValue);
		} catch (Exception e) {
			LOGGER.error("Could not set value '" + propertyValue + "' for field " + target.getClass().getName() + ":"
					+ field.getName(), e);
		}
	}

	private ArzRecordProperty getProperty(ArzRecord record, String propertyNameValue) {
		ArzRecordProperty recordProperty = record.getProperties().stream()
				.filter(property -> property.getName().equals(propertyNameValue)).findFirst().get();
		return recordProperty;
	}

	private Object mapComplexProperty(Class<?> complexPropertyType, ArzRecordProperty property)
			throws InstantiationException, IllegalAccessException {
		Object mappedComplexProperty = complexPropertyType.newInstance();
		for (Field propertyField : complexPropertyType.getDeclaredFields()) {
			propertyField.setAccessible(true);
			if (propertyField.isAnnotationPresent(PropertyName.class)) {
				propertyField.set(mappedComplexProperty, property.getName());
			} else if (propertyField.isAnnotationPresent(PropertyValue.class)) {
				propertyField.set(mappedComplexProperty, property.getValue());
			}
		}
		return mappedComplexProperty;
	}
}
