package de.elementil.arzreader;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableInt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import de.elementil.arzreader.model.ArzFile;
import de.elementil.arzreader.model.ArzRecord;
import de.elementil.arzreader.model.ArzRecordProperty;
import de.elementil.arzreader.model.ArzString;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;

public class ArzReader {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArzReader.class);

	private static final int RECORD_DATA_BLOCK_START_ADDRESS = 24;

	private ByteBuffer data;

	/**
	 * Reads the data of an .arz file into the given {@link ArzFile} object
	 * 
	 * @throws IOException
	 *             if the file for the given path could not be opened
	 */
	public void read(ArzFile arzFile) throws IOException {
		arzFile.getRecords().clear();
		LOGGER.info("Started reading " + arzFile.getFile().getName() + "...");
		long start = System.currentTimeMillis();
		data = readFileIntoByteBuffer(arzFile.getFile());

		try {
			readHeader(arzFile);
			readStringTable(arzFile);
			readRecordTable(arzFile);
		} catch (BufferUnderflowException e) {
			LOGGER.error("File data is corrupt.", e);
			throw new RuntimeException("File data is corrupt.", e);
		}

		data = null;
		long end = System.currentTimeMillis();
		LOGGER.info("Finished reading " + arzFile.getFile().getName() + ". Time elapsed: " + (end - start) + " miliseconds.");
	}

	/**
	 * Dumps the .dbr files read from the .arz file into the a sub folder with
	 * the name 'extracted' on the current path.
	 * 
	 * @throws RuntimeException
	 *             If an IOException occurs. The IOException is wrapped in the
	 *             RuntimeException.
	 */
	public void dump(ArzFile arzFile) {
		dump(arzFile, Paths.get(".\\extracted"));
	}

	/**
	 * Dumps the .dbr files read from the .arz file into the given path
	 * 
	 * @throws RuntimeException
	 *             If an IOException occurs. The IOException is wrapped in the
	 *             RuntimeException.
	 */
	public void dump(ArzFile arzFile, Path path) {
		LOGGER.info("Started dumping to filesystem...");
		long start = System.currentTimeMillis();
		arzFile.getRecords().parallelStream().forEach(record -> createDbrFile(path, record));
		long end = System.currentTimeMillis();
		LOGGER.info("Finished dumping to filesystem. Time elapsed: " + (end - start) + " miliseconds.");
	}

	/**
	 * Dumps the .dbr of the given {@link ArzRecord} into the given path
	 * 
	 * @throws RuntimeException
	 *             If an IOException occurs. The IOException is wrapped in the
	 *             RuntimeException.
	 */
	public void createDbrFile(Path path, ArzRecord record) {
		LOGGER.info("Writing file " + record.getFileName() + "...");
		Path resolvedPath = path.resolve(record.getFileName()).toAbsolutePath().normalize();
		try {
			Path pathWithoutFile = resolvedPath.getParent();
			if (pathWithoutFile != null) {
				Files.createDirectories(pathWithoutFile);
			}
			File dbr = resolvedPath.toFile();
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.writeValue(dbr, record);
		} catch (IOException e) {
			LOGGER.error("Exception while trying to create " + record.getFileName(), e);
			throw new RuntimeException(e);
		}
	}

	private ByteBuffer readFileIntoByteBuffer(File file) throws IOException {
		try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r")) {
			try (FileChannel inChannel = randomAccessFile.getChannel()) {
				ByteBuffer data = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size()).order(ByteOrder.LITTLE_ENDIAN);
				return data;
			}
		}
	}

	private void readHeader(ArzFile arzFile) {
		arzFile.setUnknown(data.getShort());
		arzFile.setVersion(data.getShort());
		arzFile.setRecordTableStart(data.getInt());
		arzFile.setRecordTableSize(data.getInt());
		arzFile.setRecordTableEntries(data.getInt());
		arzFile.setStringTableStart(data.getInt());
		arzFile.setStringTableSize(data.getInt());

		if (arzFile.getUnknown() != 2 || arzFile.getVersion() != 3) {
			LOGGER.error("File " + arzFile.getFile().getName() + " is not a Grim Dawn database.arz file.");
			throw new IllegalArgumentException("File " + arzFile.getFile().getName() + " is not a Grim Dawn database.arz file.");
		}
	}

	private void readStringTable(ArzFile arzFile) {
		data.position(arzFile.getStringTableStart());

		int count = data.getInt();
		ArrayList<ArzString> strings = new ArrayList<ArzString>(count);
		for (int i = 0; i < count; i++) {
			strings.add(readNextString());
		}
		arzFile.setStrings(strings);
	}

	private ArzString readNextString() {
		int length = data.getInt();
		byte[] stringBuffer = new byte[length];
		data.get(stringBuffer);
		return new ArzString(new String(stringBuffer), length);
	}

	private void readRecordTable(ArzFile arzFile) {
		data.position(arzFile.getRecordTableStart());

		List<ArzRecord> records = new ArrayList<ArzRecord>(arzFile.getRecordTableEntries());
		ArzRecord record;
		MutableInt index = new MutableInt(data.position());
		for (int i = 0; i < arzFile.getRecordTableEntries(); i++) {
			record = readRecord(arzFile, index);
			records.add(record);
		}

		records.stream().forEach(r -> initRecord(arzFile, r));
		arzFile.setRecords(records);
	}

	private ArzRecord readRecord(ArzFile arzFile, MutableInt index) {
		data.position(index.intValue());
		ArzRecord record = new ArzRecord();
		record.setFileName(arzFile.getString(data.getInt()));
		record.setTypeLength(data.getInt());
		byte[] stringBuffer = new byte[record.getTypeLength()];
		data.get(stringBuffer);
		record.setType(new String(stringBuffer));
		record.setDataOffset(data.getInt());
		record.setDataSizeCompressed(data.getInt());
		record.setDataSizeDecompressed(data.getInt());

		// move index to the start index of the next record
		// TODO: figure out what those 8 bytes are
		index.setValue(data.position() + 8);

		return record;
	}

	private void initRecord(ArzFile arzFile, ArzRecord record) {
		if (!record.isInitialized()) {
			record.setProperties(readRecordProperties(arzFile, record.getDataOffset(), record.getDataSizeCompressed(), record.getDataSizeDecompressed()));
		}
	}

	@SuppressWarnings("unchecked")
	private List<ArzRecordProperty> readRecordProperties(ArzFile arzFile, int dataOffset, int sizeCompressed, int sizeDecompressed) {
		ByteBuffer decompressed = decompressRecordData(dataOffset, sizeCompressed, sizeDecompressed).order(ByteOrder.LITTLE_ENDIAN);
		decompressed.position(0);

		List<ArzRecordProperty> recordProperties = new ArrayList<ArzRecordProperty>();
		ArzRecordProperty property;
		int current = 0;
		int propertyLengthInBytes;
		while (current < sizeDecompressed) {
			short type = decompressed.getShort();
			short count = decompressed.getShort();
			int string = decompressed.getInt();

			property = new ArzRecordProperty(arzFile.getString(string));

			Object value = null;
			Object currentValue;
			if (count > 1) {
				value = new ArrayList<Object>();
			}
			for (int i = 0; i < count; i++) {
				switch (type) {
				case 1:
					currentValue = decompressed.getFloat();
					break;
				case 2:
					currentValue = arzFile.getString(decompressed.getInt());
					break;
				default:
					currentValue = decompressed.getInt();
					break;
				}

				if (value instanceof ArrayList) {
					((ArrayList<Object>) value).add(currentValue);
				} else {
					value = currentValue;
				}
			}

			property.setValue(value);
			recordProperties.add(property);

			propertyLengthInBytes = 8 + count * 4;
			current += propertyLengthInBytes;
		}

		return recordProperties;
	}

	private synchronized ByteBuffer decompressRecordData(int dataOffset, int sizeCompressed, int sizeDecompressed) {
		int position = data.position();
		data.position(RECORD_DATA_BLOCK_START_ADDRESS + dataOffset);

		byte[] compressed = new byte[sizeCompressed];
		byte[] decompressed = new byte[sizeDecompressed];
		ByteBuffer compressedBuffer = ByteBuffer.wrap(compressed);
		ByteBuffer decompressedBuffer = ByteBuffer.wrap(decompressed);

		data.get(compressed);

		LZ4Factory factory = LZ4Factory.fastestInstance();
		LZ4FastDecompressor decompressor = factory.fastDecompressor();
		decompressor.decompress(compressedBuffer, decompressedBuffer);

		data.position(position);

		return decompressedBuffer;
	}
}
