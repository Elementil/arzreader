package de.elementil.arzreader;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.elementil.arzreader.model.ArzRecord;
import de.elementil.arzreader.model.ArzRecordProperty;

public class RecordFinder {

	public class PropertyCriteria {
		private Predicate<ArzRecordProperty> propertyPredicate;

		PropertyCriteria() {
			propertyPredicate = property -> property != null;
		}
		
		PropertyCriteria(String propertyName) {
			propertyPredicate = property -> property.getName().toLowerCase().equals(propertyName.toLowerCase());
		}		

		public RecordFinder isEqual(Object value) {
			Predicate<ArzRecordProperty> valueEquals = property -> property.getValue().equals(value);
			return fullfillsPredicate(valueEquals);
		}

		public RecordFinder isGreaterThan(float value) {
			Predicate<ArzRecordProperty> valueIsGreaterThan = property -> greaterThan(property, value);
			return fullfillsPredicate(valueIsGreaterThan);
		}

		public RecordFinder isSmallerThan(float value) {
			Predicate<ArzRecordProperty> valueIsGreaterThan = property -> smallerThan(property, value);
			return fullfillsPredicate(valueIsGreaterThan);
		}

		public RecordFinder contains(String value) {
			Predicate<ArzRecordProperty> valueContains = property -> valueContains(property, value);
			return fullfillsPredicate(valueContains);
		}

		public RecordFinder fullfillsPredicate(Predicate<ArzRecordProperty> valueComparison) {
			Predicate<ArzRecord> propertyValueEquals = record -> record.getProperties().stream().filter(propertyPredicate.and(valueComparison)).count() > 0;			
			return RecordFinder.this.and(propertyValueEquals);
		}

		private boolean greaterThan(ArzRecordProperty property, float value) {
			if (property.getValue() instanceof Number) {
				return ((Number) property.getValue()).floatValue() > value;
			}
			return false;
		}

		private boolean smallerThan(ArzRecordProperty property, float value) {
			if (property.getValue() instanceof Number) {
				return ((Number) property.getValue()).floatValue() < value;
			}
			return false;
		}

		private boolean valueContains(ArzRecordProperty property, String value) {
			if (property.getValue() instanceof String) {
				return ((String) property.getValue()).toLowerCase().contains(value.toLowerCase());
			}
			return false;
		}
	}

	Predicate<ArzRecord> predicate;
	List<ArzRecord> records;

	public RecordFinder(List<ArzRecord> records) {
		this(records, isNotNull());
	}

	private RecordFinder(List<ArzRecord> records, Predicate<ArzRecord> predicate) {
		this.records = records;
		this.predicate = predicate;
	}

	public RecordFinder recordPath(String recordPath) {
		return and(hasExactRecordPath(recordPath));
	}
	
	public RecordFinder recordPathStartsWith(String recordPath) {
		return and(hasRecordPathStartingWith(recordPath));
	}

	public RecordFinder recordName(String recordName) {
		return and(hasRecordName(recordName));
	}

	public PropertyCriteria property(String propertyName) {
		return new PropertyCriteria(propertyName);
	}
	
	public PropertyCriteria anyProperty() {
		return new PropertyCriteria();
	}

	public RecordFinder and(Predicate<ArzRecord> predicate) {
		return new RecordFinder(records, this.predicate.and(predicate));
	}

	public List<ArzRecord> find() {
		List<ArzRecord> result = records.stream().filter(predicate).collect(Collectors.toList());
		return result;
	}

	private static Predicate<ArzRecord> isNotNull() {
		return record -> record != null;
	}

	private static Predicate<ArzRecord> hasRecordName(String fileName) {
		return record -> record.getFileName().substring(record.getFileName().lastIndexOf("/"), record.getFileName().length()).equals(fileName);
	}

	private static Predicate<ArzRecord> hasExactRecordPath(String recordPath) {
		return record -> record.getFileName().substring(0, record.getFileName().lastIndexOf("/")).equals(recordPath);
	}
	
	private static Predicate<ArzRecord> hasRecordPathStartingWith(String recordPath) {
		return record -> record.getFileName().startsWith(recordPath);
	}
}
